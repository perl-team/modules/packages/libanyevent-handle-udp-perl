libanyevent-handle-udp-perl (0.050-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info format in d/source/lintian-overrides on line
    2-7.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 15:18:49 +0000

libanyevent-handle-udp-perl (0.050-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 04 Jul 2022 17:51:41 +0100

libanyevent-handle-udp-perl (0.050-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.0
  * copyright: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Apr 2020 21:23:25 +0200

libanyevent-handle-udp-perl (0.049-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Fix regular expression.
    + Rewrite usage comment.
    + Use substitution strings.
  * Simplify rules.
    Stop build-depend on cdbs.
  * Fix hashbang of example file.
  * Update copyright info: Extend coverage of packaging.
  * Stop (build-)depend on libnamespace-clean-perl.
  * Mark build-dependencies needed only for testsuite as such.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Explain AnyEvent in long description.
  * Declare compliance with Debian Policy 4.3.0.
  * Stop build-depend on dh-buildinfo.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 18 Feb 2019 21:34:08 +0100

libanyevent-handle-udp-perl (0.048-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update watch file: Tighten to ignore prereleases.
  * Modernize Vcs-Browser field: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
      Relax to build-depend unversioned on cdbs.
      Stop build-depend on licensecheck.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Extend coverage for myself.
  * Stop build-depend on libmoo-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 29 Aug 2017 01:29:33 +0200

libanyevent-handle-udp-perl (0.044-1) unstable; urgency=medium

  [ upstream ]
  * New release:
    + Avoid resending entry if object was destroyed.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump to version 4. Use MetaCPAN URL.
    + Mention uscan and gpb in usage comment.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Update git-buildpackage config: Filter any .gitignore file.
  * Stop track upstream source with CDBS (use gpb --uscan).
  * Build-depend on licensecheck (not devscripts).
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize Vcs-Git field: Use https URL.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Sep 2016 17:46:54 +0200

libanyevent-handle-udp-perl (0.043-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * mark package as autopkg-testable
  * use canonical Vcs-Git URL
  * Declare conformance with Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Sun, 22 Nov 2015 07:36:14 +0000

libanyevent-handle-udp-perl (0.043-1) unstable; urgency=medium

  [ upstream ]
  * New release:
  * Fix compile tests

  [ Jonas Smedegaard ]
  * Build-depend on libfile-spec-perl.
  * Bump standards-version to 3.9.5.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Apr 2014 17:29:13 +0200

libanyevent-handle-udp-perl (0.042-1) unstable; urgency=low

  [ upstream ]
  * New release(s):
    + Fix error handling in a number of places.
    + Handle address reuse better.
    + Require Moo 1.001000.
    + Allow EINTR in recv.

  [ Jonas Smedegaard ]
  * Update copyright file:
    + Drop irrelevant (and unusually shortnamed) APGL-3 License section.
      Thanks to Ansgar Burchardt.
  * Update README.source to emphasize control.in file as *not* a
    show-stopper for contributions.
  * Add github URL as alternate source.
  * Stop track md5sum of upstream tarball.
  * Improve watch file to use both www.cpan.org/authors URL (for newest
    info) and search.cpan.org/dist URL (for change of author).
    Switch get-orig-source target to use www.cpan.org/authors URL.
  * Tighten to (build-)depend versioned on libmoo-perl.
  * Adapt to upstream switch to Module::Build::Tiny:
    + Build-depend on libmodule-build-tiny-perl.
    + Tighten to build-depend versioned on cdbs.
    + Adapt CDBS includes.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 30 Sep 2013 12:01:01 +0200

libanyevent-handle-udp-perl (0.039-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#703491.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Mar 2013 11:24:13 +0100
